import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  jobs: Object;
  searchText: any;

  constructor(private dataService: DataService){}

  findJob(){
    this.dataService.updateSearch(this.searchText);
    this.dataService.getJobs().subscribe(
      data => {
        this.jobs = data;
        console.log(data);
        
      });
  }
  ngOnInit(){
  }
}
