import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";


@Injectable({
	providedIn: "root"
})
export class DataService {
    private searchText:string;

	constructor(private _http: HttpClient) {}
	getJobs() {
        return this._http.get("https://cors-anywhere.herokuapp.com/https://jobs.github.com/positions.json?search=" + this.searchText);
    }

    updateSearch(input){
        this.searchText = input;
    }

}